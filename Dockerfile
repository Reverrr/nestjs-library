FROM node:8
WORKDIR /app
COPY . /app/
COPY package*.json ./
RUN rm -rf dist && rm -rf node_modules && npm install -g @nestjs/cli && npm install && npm rebuild node-sass --force && dir && rm -rf build && npm run build
EXPOSE 8000
CMD ["npm", "start"]