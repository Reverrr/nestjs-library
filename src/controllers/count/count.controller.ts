//Vendors
import { Controller, Get, Res, HttpStatus } from '@nestjs/common';
import { Response } from 'express';
//Interfaces
import { CountInterface } from 'src/models';
//Services
import { CountService } from 'src/services/count.service';

@Controller('count')
export class CountController {
  constructor(private readonly countService: CountService) {}

  @Get()
  public getCount(@Res() res: Response): void{
    this.countService.getCount()
    .then(
      (data) => {
        res.status(HttpStatus.OK).json(data);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  
}