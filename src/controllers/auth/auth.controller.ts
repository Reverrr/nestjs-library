//Vendors
import { Controller, Get, Post, Req, HttpStatus, Res, Headers } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Response, Request } from 'express';
//Services
import { AuthService } from 'src/services/auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('verify')
	@ApiBearerAuth()
	public verify(@Headers('authorization') token: string, @Res() res: Response): Promise<void> {
    console.log(321);
    return this.authService.verifyToken(token)
    .then(
      (data) => {
        res.status(HttpStatus.OK).json(data);
      },
      () => {
        res.status(HttpStatus.UNAUTHORIZED).json(HttpStatus.UNAUTHORIZED);
      }
    );
	}

  @Post('login')
  public async login(@Req() req: Request, @Res() res: Response): Promise<void>  {
    console.log(123);

    return await this.authService.createToken(req.body)
    .then(
      async (data) => {
        await res.json(data);
      }
    );
  }
}