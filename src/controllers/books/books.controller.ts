//Vendors
import { Controller, Get, Post, Body, Query, Res, HttpStatus, Param, Put, Delete } from '@nestjs/common';
import { Response } from 'express';
//Interfaces
import { BookInterface, KeyValueInterface, QueryInterface } from 'src/models';
//Services
import { BookService } from 'src/services/books.service';
import { CountService } from 'src/services/count.service';
//Dto
import { CreateBook } from 'src/dto/book-create.dto';

@Controller('books')
export class BooksController {
  constructor(
    private bookService: BookService,
    private countService: CountService
    ) {}

  @Post()
  public async create(@Body() createBook: CreateBook) {
    this.countService.increaseCountBooks();
    await this.bookService.create(createBook);
  }

  @Get()
  public async findAll(@Query() query: QueryInterface, @Res() res: Response): Promise<void> {
    this.bookService.findByFilter(query.q, query._limit, query.authors, query.type, query.page)
      .then(
          (data: BookInterface[]) => {
            res.status(HttpStatus.OK).json(data);
          },
          (err) => {
            res.status(HttpStatus.NO_CONTENT);
          },
      );
  }

  @Get(':id')
  public async findOneById(@Param() params: KeyValueInterface<string>, @Res() res: Response): Promise<void> {
    this.bookService.getBookById(params.id)
    .then(
        (data: BookInterface) => {
            res.status(HttpStatus.OK).json(data);
        }
    );
  }
  
  @Put(':id')
  public async changeBookById(@Param() params: KeyValueInterface<string>, @Body() body): Promise<BookInterface[]> {
    return this.bookService.changeBookById(params.id, body);
  }

  @Delete(':id')
  public async deleteBook(@Param() params: KeyValueInterface<string>): Promise<BookInterface[]> {
    this.countService.reduceCountBooks();
    return this.bookService.deleteBookById(params.id);
  }
}