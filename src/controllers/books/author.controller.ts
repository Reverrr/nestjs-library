//Vendors
import { Controller, Get, Post, Body, Res, HttpStatus, Delete, Param } from '@nestjs/common';
import { Response } from 'express';
//Interfaces
import { AuthorInterface, KeyValueInterface } from 'src/models';
//Dto
import { CreateAuthor } from 'src/dto/author-create.dto';
//Services
import { BookService } from 'src/services/books.service';

@Controller('authors')
export class AuthorsController {
  constructor(private bookService: BookService) {}

  @Post()
  public async createAuthor(@Body() createAuthor: CreateAuthor, @Res() res: Response) {
    await this.bookService.createAuthor(createAuthor)
    return res.status(HttpStatus.CREATED).json(createAuthor)
  }

  @Delete(':id')
  public async deleteBook(@Param() params: KeyValueInterface<string>, @Res() res: Response) {
    this.bookService.deleteAuthor(params.id);
    return res.status(HttpStatus.CREATED).json({status: HttpStatus.CREATED});
  }

  @Get()
  public async getAuthors(@Res() res: Response): Promise<void> {
      this.bookService.getAuthors()
      .then(
          (data: AuthorInterface[]) => {
              res.status(HttpStatus.OK).json(data);
          },
          (err) => {
            res.status(HttpStatus.NO_CONTENT);
              
          }
      )
  }

}