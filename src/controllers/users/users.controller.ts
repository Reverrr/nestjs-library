//Vendors
import { Controller, Get, Post, Body, Res, HttpStatus, Query, Delete, Param, Put } from '@nestjs/common';
import { Response } from 'express';
//Interfaces
import { UserInterface, QueryInterface, KeyValueInterface, UserChangePasswordInterface } from 'src/models';
//Services
import { CountService } from 'src/services/count.service';
import { UserService } from 'src/services/user.service';
//Dto
import { CreateUser } from 'src/dto/user-create.dto';

@Controller('users')
export class UsersController {
  constructor(
    private userService: UserService,
    private readonly countService: CountService
    ) {}

  @Post()
  public create(@Body() createUser: CreateUser, @Res() res: Response): void {
    this.userService.createUser(createUser)
    .then(
      (done) => {
        this.countService.increaseCountUsers();
        res.status(HttpStatus.CREATED).json({ status: HttpStatus.CREATED });
      },
      (err) => {
        if(err.code === 11000) {
          res.status(HttpStatus.FORBIDDEN).json({
            status: HttpStatus.FORBIDDEN,
            message: HttpStatus.FORBIDDEN
          });
        }
      }
    );
  }

  @Get()
  public async findAll(@Query() query: QueryInterface): Promise<UserInterface[]> {
    return this.userService.findAll(query.page, query._limit);
  }
  
  @Get(':id')
  public async getUserById(@Param() params: KeyValueInterface<string>): Promise<UserInterface[]> {
    return this.userService.getUserById(params.id);
  }

  @Put(':id')
  public async changeUserById(@Param() params: KeyValueInterface<string>, @Body() body: UserInterface): Promise<UserInterface[]> {
    return this.userService.changeUserById(params.id, body);
  }

  @Delete(':id')
  public async deleteUser(@Param() params: KeyValueInterface<string>): Promise<UserInterface[]> {
    this.countService.reduceCountUsers();
    return this.userService.deleteUserById(params.id);
  }
  
  @Post('changeUserPassword')
  public async changeUserPassword(@Body() body: UserChangePasswordInterface, @Res() res: Response) {
    this.userService.changeUserPassword(body).then(
      (data) => {
        res.status(HttpStatus.CREATED).json({status: HttpStatus.CREATED, data: data});
        return data;
      },
      (err) => {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({status: HttpStatus.INTERNAL_SERVER_ERROR});
      }
    )
  }
}