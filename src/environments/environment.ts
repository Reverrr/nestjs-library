export const environment = {
    mongoDBUrl: 'mongodb://localhost:27017',
    originCORS: [
        'http://localhost:4200',
        'http://localhost:3001',
        'http://localhost:8100'
      ],
  mongodb: {
    url: 'mongodb+srv://test:test@library-x2suc.mongodb.net/test?retryWrites=true',
    host: 'localhost',
    database: 'blogDB',
  },
    secretOrPrivateKey: 'ASdadcsaASDASSSsssAXL',
    startNewBuild: '================================NEW BUILD================================'
  };