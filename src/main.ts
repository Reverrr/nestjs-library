//Vendors
import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import { ValidationPipe } from '@nestjs/common';
//Modules
import { AppModule } from './app.module';
//Environment
import { environment } from 'src/environments/environment';
//Filter
import { AllExceptionsFilter } from 'src/filters/http-exception.filter';
//Logger
import { MyLogger } from 'src/logger';
//Services
import { LimitService } from './services/limit.service';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const limit = new LimitService();

async function bootstrap() {
  const app = await NestFactory.create(AppModule,
    {
      logger: false,
      cors: true
      // {
      //   origin: environment.originCORS,
      //   methods: 'GET,HEAD,OPTIONS,POST,PUT, DELETE',
      //   allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
      // }
    }
  );

  const options = new DocumentBuilder()
  .setTitle('Lib example')
  .setDescription('description')
  .setVersion('1.0')
  .build();
const document = SwaggerModule.createDocument(app, options);
SwaggerModule.setup('api', app, document);

  limit.init(app.use);
  app.use(bodyParser.json({ limit: '20mb' }));
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new AllExceptionsFilter());
  app.useLogger(app.get(MyLogger));
  app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));
  await app.listen(8000);
}
bootstrap();
