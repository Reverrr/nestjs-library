//Vendors
import { IsNotEmpty, IsAlpha, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateBook {
    @ApiModelProperty()
    @IsArray()
    @IsNotEmpty()
    readonly author: string[];

    @ApiModelProperty()
    @IsNotEmpty()
    readonly name: string;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly description: string;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly img: string[];

    @ApiModelProperty()
    @IsNotEmpty()
    @IsAlpha()
    readonly type: string;
}