//Vendors
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsAlpha } from 'class-validator';

export class CreateUser {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsAlpha()
    firstName: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsAlpha()
    readonly lastName: string;
    @ApiModelProperty()
    @IsNotEmpty()
    readonly username: string;
    @ApiModelProperty()
    @IsNotEmpty()
    password: string;
    @ApiModelProperty()
    @IsNotEmpty()
    @IsAlpha()
    readonly role: string;
    @ApiModelProperty()
    readonly createdAt: Date;
    @ApiModelProperty()
    readonly updatedAt: Date;
}