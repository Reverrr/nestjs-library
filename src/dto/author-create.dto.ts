//Vendors
import { IsNotEmpty, IsAlpha } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateAuthor {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsAlpha()
    readonly name: string;
}