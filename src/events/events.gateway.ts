import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect,
} from '@nestjs/websockets';
import { ChatInterface } from 'src/models';

@WebSocketGateway(8077)
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;
    public users: number = 0;

    async handleConnection(client: any) {
        this.users++;
        /**
         * Notify connected clients of current users
         */
        this.server.emit('users', this.users);
    }

    async handleDisconnect() {
        this.users--;
        this.server.emit('users', this.users);
    }

    @SubscribeMessage("events")
    handleEvent(client: any, data: ChatInterface): WsResponse<ChatInterface> {
        const event = 'events'; 
        client.broadcast.emit('events', data);
        return { event, data };
    }
}