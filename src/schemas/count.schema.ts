//Vendors
import * as mongoose from 'mongoose';

export const CountSchema = new mongoose.Schema({
    totalCountBooks: {
        type: Number
    },
    totalCountUsers: {
        type: Number
    }
});