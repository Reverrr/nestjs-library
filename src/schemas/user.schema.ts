//Vendors
import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
     firstName: String,
     lastName: String,
     username: {
        type: String,
        unique: true
    },
     password: String,
     role: String,
     createdAt: Date,
     updatedAt: Date,
});