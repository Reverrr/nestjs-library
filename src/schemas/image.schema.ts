//Vendors
import * as mongoose from 'mongoose';

export const ImageSchema = new mongoose.Schema({
    filename: String,
    filetype: String,
    value: String
});