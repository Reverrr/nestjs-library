//Vendors
import * as mongoose from 'mongoose';
import { AuthorSchema } from 'src/schemas/author.schema';

export const BookSchema = new mongoose.Schema({
     author: [AuthorSchema],
     name: String,
     description: String,
     password: String,
     img: String,
     type: String,
});