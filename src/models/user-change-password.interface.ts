//Interfaces
import { UserInterface } from './user.interface';

export interface UserChangePasswordInterface {
    currentPassword: string;
    newPassword: string;
    userModel: UserInterface
  }