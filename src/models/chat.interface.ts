export interface ChatInterface extends Document {
  readonly username: string;
  readonly message: string;
  time: string;
}