//Vendors
import { Document } from 'mongoose';

export interface BookInterface extends Document {
  readonly author: string[];
  readonly name: string;
  readonly description: string;
  readonly password: string;
  readonly img: string;
  readonly type: string;
}