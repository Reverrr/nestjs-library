//Vendors
import { Document } from 'mongoose';

export interface CountInterface extends Document {
  readonly totalCountBooks: string;
  readonly totalCountUsers: string;
  readonly id: string;
}