//Vendors
import { Document } from 'mongoose';

export interface QueryInterface extends Document {
  readonly q: string;
  readonly _limit: number;
  readonly authors: string;
  readonly type: string;
  readonly page: number;
}