//Vendors
import { Document } from 'mongoose';

export interface UserInterface extends Document {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  readonly role: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}