//Vendors
import { Injectable } from '@nestjs/common';
import * as rateLimit from 'express-rate-limit';

@Injectable()
export class LimitService {
  public limitedChangePass = rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 3,
    message:
    "Too many try changed password from this IP, please try again after an 15 min",
  });

  constructor() {}
  
  public init(callBack: Function): void {
    callBack("/users/changeUserPassword/", this.limitedChangePass);
  }
}