//Vendors
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
//Interfaces
import { UserInterface, JwtPayload } from 'src/models';
//Services
import { UserService } from 'src/services/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService
  ) { }

  public async createToken(body: UserInterface) {
    const isVerifiData: boolean = await this.userService.verifyPassword(body);
    if (isVerifiData) {
      const user: JwtPayload = { username: body.username };
      const accessToken = this.jwtService.sign(user, { expiresIn: 3600 });
      return {
        expiresIn: 3600,
        accessToken,
      };
    } else {
      return false;
    }
  }

  public async verifyToken(tokenPath: string) {
    if (!!tokenPath) {
      const token = tokenPath.split(' ')[1];
      const decodeToken = this.jwtService.verify(token);
      const user: JwtPayload = { username: decodeToken.username };
      const accessToken = this.jwtService.sign(user, { expiresIn: 3600 });
      return await this.userService.getUserByUsername(decodeToken.username, accessToken);
    } else {
      throw Error()
    }
  }
}