//Vendors
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
//Interfaces
import { UserChangePasswordInterface, UserInterface } from 'src/models';
//Dto
import { CreateUser } from 'src/dto/user-create.dto';

@Injectable()
export class UserService {

    constructor(@InjectModel('User') private readonly userModel: Model<UserInterface>) { }

    public async createUser(crt: CreateUser): Promise<UserInterface> {
        const hash = await bcrypt.hash(crt.password, 10);
        crt.password = hash;
        const createdUser = new this.userModel(crt);
        return await createdUser.save();
    }

    public async findAll(page: number, limit: number): Promise<UserInterface[]> {
        return await this.userModel.find()
            .skip((+page - 1) * +limit).limit(+limit).exec();
    }

    public async deleteUserById(id: string): Promise<UserInterface[]> {
        return await this.userModel.remove({ _id: id });
    }

    public async getUserById(id: string): Promise<UserInterface[]> {
        return await this.userModel.findById(id);
    }

    public async changeUserById(id: string, body: UserInterface): Promise<UserInterface[]> {
        return await this.userModel.findOneAndUpdate({ _id: id }, body);
    }

    public async getUserByUsername(username: string, token: string): Promise<UserInterface[]> {
        return await this.userModel.findOne({ username: username }).exec()
            .then(
                (data) => {
                    return { userModel: data, newToken: token };
                }
            )
    }

    public verifyPassword(body: UserInterface): Promise<boolean> {
        return this.userModel.findOne({ username: body.username }).exec()
            .then(
                async (data) => {
                    if (data) {
                        const matchPasswords = await bcrypt.compare(body.password, data.password);
                        return await matchPasswords;
                    } else {
                        return await false;
                    }
                },
                async (err) => {
                    return await false;
                }
            );
    }
    public async changeUserPassword(body: UserChangePasswordInterface): Promise<UserInterface[]> {
        body.userModel.password = body.currentPassword;
        return this.verifyPassword(body.userModel).then(
            async (data) => {
                if (data) {
                    const hash = await bcrypt.hash(body.newPassword, 10);
                    body.userModel.password = hash;
                    return await this.userModel.findOneAndUpdate({ username: body.userModel.username }, body.userModel);
                } else {
                    throw Error('Error');
                }
            }
        );
    }
}