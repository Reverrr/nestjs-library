//Vendors
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
//Interfaces
import { CountInterface } from 'src/models';

@Injectable()
export class CountService {

  constructor(@InjectModel('Count') private readonly countModel: Model<CountInterface>) {}

  public async getCount() {
    return await this.countModel.find().exec();
  }

  public async increaseCountUsers(): Promise<void> {
    this.countModel.findOneAndUpdate({id: 'countValue'}, { $inc: { totalCountUsers: +1 } }, { new: true }, (err, count) => {});
  }

  public async reduceCountUsers(): Promise<void> {
    this.countModel.findOneAndUpdate({id: 'countValue'}, { $inc: { totalCountUsers: -1 } }, { new: true }, (err, count) => {});
  }

  public async increaseCountBooks(): Promise<void>{
    this.countModel.findOneAndUpdate({id: 'countValue'}, { $inc: { totalCountBooks: +1 } }, { new: true }, (err, count) => {});
  }

  public async reduceCountBooks(): Promise<void> {
    this.countModel.findOneAndUpdate({id: 'countValue'}, { $inc: { totalCountBooks: -1 } }, { new: true }, (err, count) => {});
  }
}