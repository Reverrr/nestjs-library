//Vendors
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
//Interfaces
import { BookInterface, AuthorInterface } from 'src/models';
//Dto
import { CreateBook } from 'src/dto/book-create.dto';
import { CreateAuthor } from 'src/dto/author-create.dto';

@Injectable()
export class BookService {
  constructor(
    @InjectModel('Book') private readonly bookModel: Model<BookInterface>,
    @InjectModel('Author') private readonly authorModel: Model<AuthorInterface>,
  ) { }

  public async create(createBook: CreateBook): Promise<BookInterface> {
    const createdBook = new this.bookModel(createBook);
    return await createdBook.save();
  }

  public async createAuthor(createAuthor: CreateAuthor): Promise<AuthorInterface> {
    const createdAuthor = new this.authorModel(createAuthor);
    return await createdAuthor.save();
  }

  public async deleteAuthor(id: string): Promise<AuthorInterface[]> {
    return await this.authorModel.deleteOne({ _id: id });
  }

  public async findAll(): Promise<BookInterface[]> {
    return await this.bookModel.find().exec();
  }
  public async findByFilter(q: string = '', _limit: number = 10, authors: string | any = '', type: string = '', page: number = 1): Promise<BookInterface[]> {
    authors.split(',')
    /**
     * Regex for search (upper and lower case)
     */
    let tempArr: any = [];
    q.split("").forEach((item: any) => { tempArr.push('(', item.toUpperCase(), '|', item.toLowerCase(), ')')});
    tempArr = tempArr.join('');
    return await this.bookModel.find({
      name: { $regex: `.*${tempArr}.*` },
      "author.name": (authors.length) ? { $all: (authors = authors.split(',')) } : /.*/,
      type: (type) ? type : /.*/
    }).limit(Number(_limit))
      .skip((+page - 1) * Number(_limit)).limit(Number(_limit))
      .exec();
  }

  public async getBookById(id: string): Promise<BookInterface> {
    return await this.bookModel.findById(id).exec();
  }

  public async deleteBookById(id: string): Promise<BookInterface[]> {
    return await this.bookModel.remove({ _id: id });
  }

  public async changeBookById(id: string, body: BookInterface): Promise<BookInterface[]> {
    return await this.bookModel.findOneAndUpdate({ _id: id }, body);
  }
  public async getAuthors(): Promise<AuthorInterface[]>  {
    return await this.authorModel.find().exec();
  }
}