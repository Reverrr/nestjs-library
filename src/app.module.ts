//Vendors
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { APP_FILTER } from '@nestjs/core';
//Controllers
import { AppController } from 'src/app.controller';
import { BooksController } from 'src/controllers/books/books.controller';
import { AuthorsController } from 'src/controllers/books/author.controller';
import { AuthController } from 'src/controllers/auth/auth.controller';
import { UsersController } from 'src/controllers/users/users.controller';
import { CountController } from 'src/controllers/count/count.controller';
//Services
import { AppService } from 'src/app.service';
import { BookService } from 'src/services/books.service';
import { AuthService } from 'src/services/auth.service';
import { UserService } from 'src/services/user.service';
import { CountService } from 'src/services/count.service';
import { LimitService } from 'src/services/limit.service';
//Schemas
import { CountSchema, UserSchema, AuthorSchema, BookSchema } from 'src/schemas';
//Socket
import { EventsGateway } from 'src/events/events.gateway';
//JWT
import { JwtStrategy } from 'src/strategy/jwt.strategy';
//Environment
import { environment } from 'src/environments/environment';
//Filter
import { AllExceptionsFilter } from 'src/filters/http-exception.filter';
//Logger
import { MyLogger } from 'src/logger';

@Module({
  imports: [
    MongooseModule.forRoot(`${environment.mongodb.url}`, {
      type: 'mongodb',
      ssl: true,
      url: environment.mongodb.url,
      username: 'test',
      password: 'test',
      database: environment.mongodb.database,
    }),
    // MongooseModule.forRoot(`${environment.mongoDBUrl}/librarydb`),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Count', schema: CountSchema }]),
    MongooseModule.forFeature([{ name: 'Book', schema: BookSchema }, {name: 'Author', schema: AuthorSchema}]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: environment.secretOrPrivateKey,
      signOptions: {
        expiresIn: 3600,
      },
    }),
  ],
  controllers: [
    AppController,
    BooksController,
    AuthController,
    AuthorsController,
    UsersController,
    CountController
  ],
  providers: [
    AppService,
    BookService,
    AuthService,
    LimitService,
    JwtStrategy,
    UserService,
    CountService,
    EventsGateway,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    MyLogger
  ]
})
export class AppModule {}
